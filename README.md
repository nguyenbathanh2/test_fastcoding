##How to run the project

**Step one:** Clone the code folder from gitlab to your device

- Choose a path to save that file -> At that path open the command window
- Run command
  > git clone https://gitlab.com/nguyenbathanh2/test_fastcoding.git

**Step two:** Run project

- cd project

- Turn on Visual Studio Code. Open the folder you just cloned to your computer.

- Install the npm dependencies

  > npm install

- Run start project

  > npm start

- Hold ctrl and click on the successfully created localhost link to view the website
